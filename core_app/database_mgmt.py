import sqlite3
 
from sqlite3 import Error
 
def sql_connection():
    try:
        con = sqlite3.connect('mydatabase.db')
        return con
    except Error:
        print(Error)
 
def sql_employee_table_create(con):
    cursorObj = con.cursor()
    cursorObj.execute("CREATE TABLE IF NOT EXISTS employees(id integer PRIMARY KEY AUTOINCREMENT, name text, age integer, department text, address text, hireDate text)")
    con.commit()

def sql_user_table_create(con):
    cursorObj = con.cursor()
    cursorObj.execute("CREATE TABLE IF NOT EXISTS users(id integer PRIMARY KEY AUTOINCREMENT, name text, username text UNIQUE, password text, createdDate text)")
    con.commit()

def sql_employee_insert(con, entities):
    cursorObj = con.cursor()  
    cursorObj.execute('INSERT INTO employees(name, age, department, address, hireDate) VALUES(?, ?, ?, ?, ?)', entities)
    con.commit()
    cursorObj.execute('DELETE FROM employees WHERE id = {}'.format(id))
    con.commit()

def sql_employee_fetch(con):
    cursorObj = con.cursor()
    cursorObj.execute('SELECT * FROM employees')
    rows = cursorObj.fetchall()
 
    for row in rows:
        print(row[1])
        # users.append(row)

def sql_users_fetch(con):
    cursorObj = con.cursor()
    cursorObj.execute("SELECT * FROM users")
    rows = cursorObj.fetchall()

    users = []

    for row in rows:
        # user.append(rows[2])
        users.append(row)
    print(users)


def sql_users_auth_check(con, username):
    cursorObj = con.cursor()
    cursorObj.execute("SELECT password FROM users WHERE username=:val",{'val':username})
    row = cursorObj.fetchall()



    for row in row:
        print(row)
    

 
con = sql_connection()
 
# #create table employee
# sql_employee_table_create(con)

#create table user
# sql_user_table_create(con)


# # insert into employee table
# emp_entities = ('Hangam', 30, 'IT', 'Kathmandu', '2018-02-06')
# msg = sql_employee_insert(con, emp_entities)


# insert into users table
# entities = ('Sagar limbu', 'sagar', 'kathmandu@123', '2020-02-06')
# sql_user_insert(con, entities)


# # delete from employee table
# sql_delete_employee_row(con, 3)


#fetch all data from employees table
# sql_employee_fetch(con)

# #fetch all data from users tablesurya

# sql_users_auth_check(con, 'sagar')
sql_users_fetch(con)


con.close()