1. Install pip
2. Create Virtualenv ( `virtualenv -p /usr/bin/python3 venv` or  `python3 -m venv venv`)
3. Activate virtualenv (`source venv/bin/activate`)
4. Go to directory of requirements.txt
5. Install dependencies (`pip install -r requirements.txt`)
6. Go to directory of stortly.py
7. Enter command (`python shortly.py` or `python3 shortly.py`)